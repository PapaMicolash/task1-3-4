package figures;

import figures.exceptions.WrongDataException;
import materials.IFilm;
import materials.IPaper;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * This is a singleton which create a box, contains methods for task
 * @author Nikolay Sizykh
 * @version 1.1
 */

public class BoxFigures implements Serializable {
    private static BoxFigures ourInstance = new BoxFigures();
    private static final long SerialVersionUID = 1l;

    /**
     * This collection keeps all figures
     */
    ArrayList<Figure> figures = new ArrayList<>();

    public static BoxFigures getOurInstance() {
        return ourInstance;
    }

    public static void setOurInstance(BoxFigures ourInstance) {
        BoxFigures.ourInstance = ourInstance;
    }

    public ArrayList<Figure> getFigures() {
        return figures;
    }

    public void setFigures(ArrayList<Figure> figures) {
        this.figures = figures;
    }

    public static BoxFigures getInstance() {
        return ourInstance;
    }

    private BoxFigures() {
    }

    /**
     * this method add first figure for create the box
     * @param figure first aded figure
     */
    public void createBox(Figure figure) {
        if (figures.size() < 1) {
            figures.add(figure);
        }
    }

    /**
     * this method add other figure
     * @param figure created figure
     */
    public void addFigures(Figure figure) {
        if (checkBox(figure)) {
            figures.add(figure);
        }
    }

    /**
     * this method checks size of box and repeating figure
     * @param figure created figure
     * @return true or false
     */
    public boolean checkBox(Figure figure) {
        boolean check = false;

        if (figures.size() < 20) {
            for (int i = 0; i < figures.size(); i++) {

                if (!figures.get(i).equals(figure)) {
                    check = true;
                } else {
                    check = false;
                }
            }
        }

        return check;
    }

    /**
     * this method return figure by its number in collection (box)
     * @param num number of figure in collection
     * @return figure
     */
    public Figure showFigureNum(int num) {
        if (num < figures.size() && num >= 0) {
            return figures.get(num - 1);
        }
        return figures.get(figures.size());
    }

    /**
     * this method extract figure from collection
     * @param num number of figure in collection
     */
    public void extractFigure(int num) {
        if (num < figures.size() && num >= 0) {
            figures.remove(num - 1);
        }
    }

    /**
     * this method replacing figure in collection by number
     * @param num number of figure in collection
     * @param figure this one replace figure in collection
     */
    public void replaceFigure(int num, Figure figure) {
        if (num < figures.size() && num >= 0) {
            figures.remove(num - 1);
            figures.add(num - 1, figure);
        }
    }

    /**
     * this method show all the figure with similar parameters
     * @param figure checking figure
     * @return collection with similar figures
     */
    public ArrayList<Figure> findFigure(Figure figure) {
        ArrayList<Figure> findFigures = new ArrayList<>();
        for (int i = 0; i < figures.size(); i++) {
            if (figure.getClass() == figures.get(i).getClass()) {
                if (Math.abs(figure.getArea() - figures.get(i).getArea()) <= 1) {
                    findFigures.add(figures.get(i));
                }
            }
        }
        return findFigures;
    }

    /**
     * this method return size of box
     * @return collection's size
     */
    public int countFigures() {
        return figures.size();
    }

    /**
     * this method calculate sum all figures' areas
     * @return areas sum
     */
    public double sumAreaFigures() {
        double sumArea = 0;
        for (int i = 0; i < figures.size(); i++) {
            sumArea += figures.get(i).getArea();
        }

        return sumArea;
    }

    /**
     * this method calculate sum all figures' perimeters
     * @return perimeters sum
     */
    public double sumPerimeterFigures() {
        double sumPerimeter = 0;
        for (int i = 0; i < figures.size(); i++) {
            sumPerimeter += figures.get(i).getPerimeter();
        }
        return sumPerimeter;
    }

    /**
     * this method return collection with circles
     * @return collection with circles from box
     */
    public ArrayList<Figure> getAllCircle() {
        ArrayList<Figure> circles = new ArrayList<>();
        for (Figure figure : figures) {
            if (figure instanceof Circle) {
                circles.add(figure);
            }
        }
        return circles;
    }

    /**
     * this method return collection with Film figures
     * @return collection with Film figures from box
     */
    public ArrayList<Figure> getAllFilmFigures() {
        ArrayList<Figure> filmFigures = new ArrayList<>();

        for (Figure figure : figures) {
            if (figure instanceof IFilm) {
                filmFigures.add(figure);
            }
        }
        return filmFigures;
    }

    /**
     * this method cutting figures from box's figures
     * @param numFigure number figure in collection
     * @param figure figure for cutting
     */
    public void cutFigure(int numFigure, Figure figure) throws WrongDataException {

        Figure cutFigure = null;

        if (figures.get(numFigure).getArea() - figure.getArea() >= 0.5) {
            double difArea = figures.get(numFigure).getArea() - figure.getArea();
            if (figure instanceof Circle) {
                if (figure instanceof IPaper) {
                    PaperCircle paperCircle = new PaperCircle(figures.get(numFigure), difArea);
                    cutFigure = paperCircle;
                } else {
                    FilmCircle filmCircle = new FilmCircle(figures.get(numFigure), difArea);
                    cutFigure = filmCircle;
                }
                figures.remove(numFigure);
                figures.add(numFigure, cutFigure);
                return;
            }

            if (figure instanceof Rectangle) {
                if (figure instanceof IPaper) {
                    PaperRectangle paperRectangle = new PaperRectangle(figures.get(numFigure), difArea);
                    cutFigure = paperRectangle;
                } else {
                    FilmRectangle filmRectangle = new FilmRectangle(figures.get(numFigure), difArea);
                    cutFigure = filmRectangle;
                }
                figures.remove(numFigure);
                figures.add(numFigure, cutFigure);
                return;
            }

            if (figure instanceof Triangle) {
                if (figure instanceof IPaper) {
                    PaperTriangle paperTriangle = new PaperTriangle(figures.get(numFigure), difArea);
                    cutFigure = paperTriangle;
                } else {
                    FilmTriangle filmTriangle = new FilmTriangle(figures.get(numFigure), difArea);
                    cutFigure = filmTriangle;
                }
                figures.remove(numFigure);
                figures.add(numFigure, cutFigure);
                return;
            }
        }
    }

    @Override
    public String toString() {
        return "BoxFigures{" +
                "figures=" + figures +
                '}';
    }
}
