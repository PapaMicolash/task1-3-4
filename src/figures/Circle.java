package figures;

import figures.exceptions.WrongDataException;

import java.io.Serializable;
import java.util.Objects;

/**
 * Abstract class Circle contains basic and specific methods and constructors
 * @author Nikolay Sizykh
 * @version 1.1
 */

public abstract class Circle extends Figure implements Serializable {

    private static final long SerialVersionUID = 2l;

    /**
     * a radius of circle
     */
    double radius;

    /**
     * basic constructor
     * @param radius a radius of circle
     */
    public Circle(double radius) throws WrongDataException {
        if (radius < 0) {
            throw new WrongDataException("Radius less then zero");
        }

        this.radius = radius;
    }

    /**
     * default constructor without parameters
     */
    public Circle() {
    }

    /**
     * this constructor for cutting circles
     * @param figure a figure for cutting
     * @param difArea a difference between areas of existing figure and new figure
     */
    public Circle(Figure figure, double difArea) {
        this.radius = Math.sqrt((figure.getArea() - difArea / Math.PI));
    }


    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    @Override
    public double getArea() {
        return Math.PI * radius * radius;
    }

    @Override
    public double getPerimeter() {
        return 2 * Math.PI * radius;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Circle)) return false;
        if (!super.equals(o)) return false;
        Circle circle = (Circle) o;
        return Double.compare(circle.radius, radius) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), radius);
    }

    @Override
    public String toString() {
        return "Circle{" +
                "radius = " + radius +
                ", area = " + getArea() +
                ", perimeter = " + getPerimeter() +
                '}';
    }
}
