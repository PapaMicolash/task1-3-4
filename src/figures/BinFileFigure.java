package figures;

import figures.exceptions.SerializaitionException;

import java.io.*;

public class BinFileFigure {
    final static String FILENAMEBIN = System.getProperty("user.dir")
            + File.separator + "figures.bin";

    public BinFileFigure() {
    }

    public void save2Bin() throws SerializaitionException {


        /*ObjectOutputStream oos = new ObjectOutputStream(fos);

        oos.writeObject(BoxFigures.getInstance());

        oos.flush();
        oos.close();*/

        try {
            FileOutputStream fos = new FileOutputStream(FILENAMEBIN);
            ObjectOutputStream oos =
                    new ObjectOutputStream(new FileOutputStream(FILENAMEBIN));
            oos.writeObject(BoxFigures.getInstance());
            oos.flush();
        }
        catch (FileNotFoundException e) {
            throw new SerializaitionException("SERILIZATION ERROR", e);
        } catch (IOException e) {
            throw new SerializaitionException("SERILIZATION ERROR", e);
        }
    }

    public void loadFromBin() throws SerializaitionException {
        try {
            FileInputStream fis = new FileInputStream(FILENAMEBIN);
            ObjectInputStream ois = new ObjectInputStream(fis);
            BoxFigures.setOurInstance((BoxFigures) ois.readObject());
            ois.close();
        } catch (FileNotFoundException e) {
            throw new SerializaitionException("SERILIZATION ERROR", e);
        } catch (IOException e) {
            throw new SerializaitionException("SERILIZATION ERROR", e);
        } catch (ClassNotFoundException e) {
            throw new SerializaitionException("SERILIZATION ERROR", e);
        }

    }
}
