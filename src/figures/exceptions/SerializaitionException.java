package figures.exceptions;

public class SerializaitionException extends Exception {

    public SerializaitionException(String message) {
        super(message);
    }

    public SerializaitionException(String message, Throwable cause) {
        super(message, cause);
    }

}
