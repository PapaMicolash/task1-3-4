package materials;

/**
 * this enum class for choice color
 *  @author Nikolay Sizykh
 *  @version 1.1
 */

public enum Color {
    BLACK, GREEN, WHITE, RED, BLUE, YELLOW, PURPLE, AZURE;
}
