package materials;

/**
 * interface for paper figures
 *  @author Nikolay Sizykh
 *  @version 1.1
 */

public interface IPaper {

    /**
     * method for checking and paint figures
     * @param color for painting
     * @return true or false
     */
    boolean paint(Color color);

    /**
     * method for getting color of figure
     * @return color
     */
    Color getColor();
}
